from django.db import models

# Create your models here.

class Jadwalku(models.Model):
	nama_kegiatan = models.CharField(max_length = 100, blank = True)
	tanggal_kegiatan = models.DateField()
	waktu_kegiatan = models.TimeField()
	tempat_kegiatan = models.CharField(max_length = 100, blank = True)
	kategori_kegiatan = models.CharField(max_length = 100, blank = True)