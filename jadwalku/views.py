from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from .forms import Form_Jadwalku
from .models import Jadwalku

# Create your views here.

def view_form(request):
	kegiatan = Jadwalku.objects.all().values()
	form = Form_Jadwalku()
	if request.method == 'POST':
		form = Form_Jadwalku(request.POST)
		if form.is_valid():
			kegiatan_baru = form.save()
			return HttpResponseRedirect(reverse('form'))
		else:
			form = Form_Jadwalku()
	return render(request, 'Form Jadwalku.html', {'form':form, 'kegiatan':kegiatan})

def delete_list(request):
	kegiatan = Jadwalku.objects.all()
	form = Form_Jadwalku()
	if request.method == 'POST':
		kegiatan.delete()
		return HttpResponseRedirect(reverse('form'))
	return render(request, 'Form Jadwalku.html', {'form':form, 'kegiatan':kegiatan})
