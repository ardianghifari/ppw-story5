from django.conf.urls import url, include
from django.urls import path
from django.contrib import admin
from .views import *

urlpatterns = [
    path('delete/', delete_list, name = 'delete'),
    path('', view_form, name = 'form')
]