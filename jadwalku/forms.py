from django import forms
from .models import Jadwalku

class Form_Jadwalku(forms.ModelForm):

	class Meta:
		model = Jadwalku
		fields = ['nama_kegiatan', 'tanggal_kegiatan', 'waktu_kegiatan', 'tempat_kegiatan', 'kategori_kegiatan']